export class GroceryItem {
    groceryItemId: number;
    name: string;
    type: string;
    quantity: number;

    constructor(groceryItemId:number, name:string, type:string, quantity:number) {
        this.groceryItemId = groceryItemId;
        this.name = name;
        this.type = type;
        this.quantity = quantity;
    }
}
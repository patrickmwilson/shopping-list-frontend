import { GroceryItem } from "./GroceryItem";

export class GroceryList {
    groceryListId: number;
    name: string;
    itemList: GroceryItem[];

    constructor(groceryListId: number, name: string, itemList: GroceryItem[]) {
        this.groceryListId = groceryListId;
        this.name = name;
        this.itemList = itemList;
    }
}
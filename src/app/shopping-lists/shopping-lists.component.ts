import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { GroceryList } from '../models/GroceryList';
import { ShoppingListService } from '../services/shopping-list.service';

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.component.html',
  styleUrls: ['./shopping-lists.component.css']
})
export class ShoppingListsComponent implements OnInit {

  constructor(private modalService: NgbModal, private listService: ShoppingListService) { }

  lName:string = "";
  status:string = "";
  closeResult = '';
  groceryLists: GroceryList[] = [];
  lFilter:string = "";

  get newListName():string{
    return this.lName;
  }

  set newListName(temp:string){
    this.lName = temp;
  }

  get listFilter():string {
    return this.lFilter;
  }

  set listFilter(temp:string) {
    this.lFilter = temp;
  }

  getFilteredGroceryLists(): GroceryList[] {
    if(this.listFilter) {
      return this.performFilter(this.listFilter);
    } else {
      return this.groceryLists;
    }
  } 

  performFilter(filterBy:string): GroceryList[] {
    filterBy = filterBy.toLowerCase();
    return this.groceryLists.filter((gList:GroceryList) => 
      gList.name.toLowerCase().indexOf(filterBy) !== -1
    );
  }

  createNewShoppingList(){
    this.listService.createGroceryList(this.newListName).subscribe(
      response => {
        var resp = JSON.parse(response);
        this.groceryLists.push(new GroceryList(resp.groceryListId, resp.name, []));
        this.newListName = "";
      }
    );
  }

  deleteShoppingList(groceryListId: number) {
    this.listService.deleteGroceryList(groceryListId).subscribe(
      response => {
        this.groceryLists = this.groceryLists.filter(groceryList => groceryList.groceryListId != groceryListId);
      }
    );
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.createNewShoppingList();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  ngOnInit(): void {
    this.listService.getAllGroceryLists().subscribe(
      response => {
        for(var groceryList of response) {
          this.groceryLists.push(new GroceryList(groceryList.groceryListId, groceryList.name, []));
        }
      }
    );
  }

}

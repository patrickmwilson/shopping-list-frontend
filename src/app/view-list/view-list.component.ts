import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { GroceryItem } from '../models/GroceryItem';
import { GroceryList } from '../models/GroceryList';
import { ShoppingListService } from '../services/shopping-list.service';

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.css']
})
export class ViewListComponent implements OnInit {

  constructor(private route: ActivatedRoute, private modalService: NgbModal, private listService: ShoppingListService) { }

  groceryListId: number;
  groceryListName: string = "";
  groceryList: GroceryList;
  itemList: GroceryItem[] = [];
  iName:string = "";
  iType:string = "";
  iQuantity:number = 0;
  closeResult = '';
  lName = '';

  iFilter:string = "";

  get itemFilter():string{
    return this.iFilter;
  }

  set itemFilter(temp:string){
    this.iFilter = temp;
  }

  get newItemName():string{
    return this.iName;
  }

  set newItemName(temp:string) {
    this.iName = temp;
  }

  get newItemType():string{
    return this.iType;
  }

  set newItemType(temp:string){
    this.iType = temp;
  }

  get newItemQuantity():number{
    return this.iQuantity;
  }

  set newItemQuantity(temp:number){
    this.iQuantity = temp;
  }

  get listName():string {
    return this.lName;
  }

  set listName(temp:string) {
    this.lName = temp;
  }

  getFilteredItems(): GroceryItem[] {
    if(this.itemFilter) {
      return this.performFilter(this.itemFilter);
    } else {
      return this.itemList;
    }
  }

  performFilter(filterBy:string): GroceryItem[] {
    filterBy = filterBy.toLowerCase();
    return this.itemList.filter((item:GroceryItem) => 
      (item.name.toLowerCase().indexOf(filterBy) != -1) ||
      (item.type.toLowerCase().indexOf(filterBy) != -1)
    );
  }

  addListItem(){
    this.listService.addItem(this.groceryListId, this.newItemName, this.newItemType, String(this.newItemQuantity)).subscribe(
      response => {
        var resp = JSON.parse(response);
        var item = new GroceryItem(resp.groceryItemId, resp.name, resp.type, resp.quantity);
        this.itemList.push(item);
        this.newItemName = "";
        this.newItemQuantity = 0;
        this.newItemType = "";
      }
    )
  }

  deleteListItem(groceryItemId:number) {
    this.listService.deleteItem(groceryItemId).subscribe(
      response => {
        this.itemList = this.itemList.filter(item => item.groceryItemId != groceryItemId);
      }
    );
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.addListItem();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {
    this.groceryListId = Number(this.route.snapshot.paramMap.get('groceryListId'));
    this.listService.findById(this.groceryListId).subscribe(
      response => {
        this.groceryListName = response.name;
        this.groceryList = new GroceryList(response.groceryListId, response.name, []);
        for(var item of response.itemList) {
          var groceryItem = new GroceryItem(item.groceryItemId, item.name, item.type, item.quantity);
          this.itemList.push(groceryItem);
        }
      }
    )
    console.log(this.groceryListId);
  }

}

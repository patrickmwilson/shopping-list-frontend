import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  constructor(private httpCli: HttpClient) { }

  baseUrl: string = "http://localhost:9025/grocery-lists";

  createGroceryList(name: String): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = {"name": name};

    return this.httpCli.post(this.baseUrl + "/create", body, {"headers": headers, responseType: "text"});
  }

  getAllGroceryLists(): Observable<any> {
    return this.httpCli.get<any>(this.baseUrl + "/all");
  }

  findById(groceryListId: number): Observable<any> {
    return this.httpCli.get<any>(this.baseUrl + "/" + groceryListId);
  }

  deleteGroceryList(groceryListId: number): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    return this.httpCli.delete(this.baseUrl + "/delete/" + groceryListId, {'headers' : headers, responseType : 'text'});
  }

  addItem(groceryListId:number, name:string, type:string, quantity:string): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = {"groceryListId" : groceryListId, "name" : name, "type" : type, "quantity" : quantity};

    return this.httpCli.post(this.baseUrl + "/items", body, {"headers" : headers, responseType: 'text'});
  }

  deleteItem(groceryItemId:number): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    return this.httpCli.delete(this.baseUrl + "/items/" + groceryItemId, {'headers' : headers, responseType : 'text'});
  }
}
